window.addEventListener( "load", eventWindowLoaded, false );

var BOX_MARGIN				= 10;
var MIN_COMMANDS_DELAY		= 150;

var Debugger = function () { };
Debugger.log = function ( message )
{
	try
	{
		console.log( message );
	}
	catch ( exception )
	{
		return;
	}
}

function eventWindowLoaded ()
{
   main();
}

function canvasSupport ()
{
	return Modernizr.canvas;
}

function s4()
{
	return Math.floor( ( 1 + Math.random() ) * 0x10000 ).toString( 16 ).substring( 1 );
}

function guid()
{
    return	( s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4() );
}

// Main
function main()
{
	// Base variables
	var theCanvas = document.getElementById( "appCanvas" );
	var context = theCanvas.getContext( "2d" );
	
	document.body.addEventListener( "touchmove", function( event ) { event.preventDefault(); }, false ); 
	
	// Window rectangle size
	var windowdWidth 				= 0;
	var windowHeight 				= 0;
	// Fill rectangle size
	var fillRectWidth 				= 0;
	var fillRectHeight 				= 0;
	// Content rectangle size
	var contentRectWidth 			= 0;
	var contentRectHeight 			= 0;
	// Coords zero point { 0, 0 }
	var xZero						= 0;
	var yZero						= 0;
	// Input coordinates
	var x = 0;
	var y = 0;
	// Controller origin coords
	var controllerOriginX 			= 0;
    var controllerOriginY 			= 0;
	// Joystick controller radius
	var controllerBorderRadius 		= 0;
	// Unit circle coordinates
	var unitCircleX					= 0;
	var unitCitcleY					= 0;
	// server URL
	var serviceURL					= "";
	// URL input
	var urlInput					= null;
	// session ID
	var sessionId					= null;
	// client ID
	var clientId					= guid();
	// Last server call timestamp
	var lastCallTimestamp			= null;
	// Connected to robot?
	var robotConnection				= false;
	
	// Add resize listener
	window.addEventListener( "resize", onResize, false );
	
	function initClientEvents()
	{
		// Add mousedown listener
		theCanvas.addEventListener( "mousedown", onMouseDown, false );
		// Add touchstart listener
		theCanvas.addEventListener( "touchstart", onTouchStart, false );
	}
	
	function initAjax()
	{
		$.support.cors = true;
		
		$.ajaxSetup(
			{
				contentType : 'application/json',
				processData : false
			});
			
		$.ajaxPrefilter( function( options, originalOptions, jqXHR )
		{
			if (options.data)
			{
				options.data=JSON.stringify(options.data);
			}
		});
	}
	
	// onResize event
	function onResize( e )
	{
		resetSizes();
		drawScreen();
	}
	
	// Resize window event
	function resetSizes()
	{
		// Window rectangle size
		windowdWidth 		= context.canvas.width  = window.innerWidth;
		windowHeight 		= context.canvas.height = window.innerHeight;
		// Fill rectangle size
/* 		fillRectWidth 		= windowdWidth - WINDOW_WIDTH_DELTA; */
/* 		fillRectHeight 		= windowHeight - WINDOW_HEIGHT_DELTA; */
		// Content rectangle size
		contentRectWidth 	= windowdWidth - BOX_MARGIN;
		contentRectHeight 	= windowHeight - BOX_MARGIN;
		
		var coordsWidthCorection = BOX_MARGIN / 2;
        var coordsHeightCorection = BOX_MARGIN / 2; 
      
        if ( contentRectHeight > contentRectWidth )
        {
        	controllerBorderRadius = contentRectWidth / 2;
        	controllerBorderRadius -= coordsWidthCorection;
        }
        else
        {
        	controllerBorderRadius = contentRectHeight / 2;
        	controllerBorderRadius -= coordsHeightCorection;
        }
        
        controllerBorderRadius -= BOX_MARGIN;
        
        xZero = contentRectWidth / 2;
        yZero = contentRectHeight / 2;
        
        x = xZero;
        y = yZero;
        
        controllerOriginX = xZero;
        controllerOriginY = yZero;
        
        unitCircleX = 0;
        unitCircleY = 0;
        
        Debugger.log( controllerBorderRadius );
	}

	/*
		---- MOUSE EVENTS ----
	*/

	// onMouseDown event
	function onMouseDown( e )
	{
		theCanvas.addEventListener( "mouseup", onMouseUp, false );
		theCanvas.addEventListener( "mousemove", onMouseMove, false );
		theCanvas.addEventListener( "mouseout", onMouseOut, false );
	
		x = e.x;
		y = e.y;
		
		x -= theCanvas.offsetLeft;
		y -= theCanvas.offsetTop;
	
/* 		Debugger.log( "onMouseDown -> x:" + x.toString() + " y:" + y.toString() ); */
		
		translateTouchToUnitCircleCoords();
		calculateControllerOrigin();
		drawScreen();
	}
	
	// onMouseUp event
	function onMouseUp ( e )
	{
		theCanvas.removeEventListener( "mouseup", onMouseUp, false );
		theCanvas.removeEventListener( "mousemove", onMouseMove, false );
		theCanvas.removeEventListener( "mouseout", onMouseOut, false );
		
		x = e.x;
		y = e.y;
		
		x -= theCanvas.offsetLeft;
		y -= theCanvas.offsetTop;
		
/* 		Debugger.log( "onMouseUpEvent -> x:" + x.toString() + " y:" + y.toString() ); */
		
		unitCircleX = 0;
		unitCitcleY = 0;
		
		resetSizes();
		drawScreen();
		sendCoords( true );
	}
	
	// onMouseOutEvent
	function onMouseOut( e )
	{
		theCanvas.removeEventListener( "mousedown", onMouseDown, false );
		theCanvas.removeEventListener( "mousemove", onMouseMove, false );
		theCanvas.removeEventListener( "mouseout", onMouseOut, false );
		
		x = e.x;
		y = e.y;
		
		x -= theCanvas.offsetLeft;
		y -= theCanvas.offsetTop;
		
/* 		Debugger.log( "onMouseUpEvent -> x:" + x.toString() + " y:" + y.toString() ); */
		
		unitCircleX = 0;
		unitCitcleY = 0;
		
		resetSizes();
		drawScreen();
		sendCoords( true );
		
		theCanvas.addEventListener( "mousedown", onMouseDown, false );
	}
	
	// onMouseMove event
	function onMouseMove( e )
	{
		x = e.x;
		y = e.y;
		
		x -= theCanvas.offsetLeft;
		y -= theCanvas.offsetTop;
	
/* 		Debugger.log( "onMouseMove -> x:" + parseInt( x ) + " y:" + parseInt( y ) ); */
		
		translateTouchToUnitCircleCoords();
		calculateControllerOrigin();
		drawScreen();
	}
	
	/*
		---- TOUCH EVENTS ----
	*/
	function onTouchStart( e )
	{
		theCanvas.removeEventListener( "touchend", onTouchEnd, false );
		theCanvas.addEventListener( "touchend", onTouchEnd, false );
		theCanvas.addEventListener( "touchmove", onTouchMove, false );
		
		if (event.targetTouches.length == 1)
		{	
		 	var touch = event.targetTouches[0];
			x = touch.pageX;
			y = touch.pageY;
			
			x -= theCanvas.offsetLeft;
			y -= theCanvas.offsetTop;
			
/* 			Debugger.log( "onTouchStart -> x:" + x.toString() + " y:" + y.toString() ); */
		}
		
		translateTouchToUnitCircleCoords();
		calculateControllerOrigin();
		drawScreen();
	}
	
	function onTouchEnd( e )
	{
		theCanvas.addEventListener( "mousedown", onMouseDown, false );
		theCanvas.removeEventListener( "touchend", onTouchEnd, false );
		theCanvas.removeEventListener( "touchmove", onTouchMove, false );
		
		if (event.targetTouches.length == 1)
		{	
		 	var touch = event.targetTouches[0];
			x = touch.pageX;
			y = touch.pageY;
			
			x -= theCanvas.offsetLeft;
			y -= theCanvas.offsetTop;
			
/* 			Debugger.log( "onTouchEnd -> x:" + x.toString() + " y:" + y.toString() ); */
		}
		
		unitCircleX = 0;
		unitCitcleY = 0;
		
		resetSizes();
		drawScreen();
		sendCoords( true );
	}
	
	function onTouchMove( e )
	{
		if (event.targetTouches.length == 1)
		{	
		 	var touch = event.targetTouches[0];
			x = touch.pageX;
			y = touch.pageY;
			
			x -= theCanvas.offsetLeft;
			y -= theCanvas.offsetTop;
			
/* 			Debugger.log( "onTouchMove -> x:" + x.toString() + " y:" + y.toString() ); */
		}
		
		translateTouchToUnitCircleCoords();
		calculateControllerOrigin();
		drawScreen();
	}
	
	function translateTouchToUnitCircleCoords()
	{
		var distance = getTouchAndJoystickMiddleDistance();
		var touchQuadrant = getCoordsQuadrant( x, y );
		
		var X = x;
		var Y = y;
		
		if ( distance >= controllerBorderRadius )
		{
			calculateControllerOrigin()
			X = controllerOriginX;
			Y = controllerOriginY;
		}
		
		var xDistance = xZero - X;
		var yDistance = yZero - Y;
		
		xDistance = Math.abs( xDistance );
		yDistance = Math.abs( yDistance );
		
		switch ( touchQuadrant )
		{
			case 1:
			{
				// Nothing to do here ... :P
			}
			break;
			
			case 2:
			{
				xDistance = -xDistance;
			}
			break;
			
			case 3:
			{
				xDistance = -xDistance;
				yDistance = -yDistance;
			}
			break;
			
			case 4:
			{
				yDistance = -yDistance;
			}
			break;
			
			default:
			break;
		}
		
		var onePercent = controllerBorderRadius / 100;

		if ( xDistance == 0 )
			unitCircleX = 0.0
		else
			unitCircleX = xDistance / onePercent;

		if ( yDistance == 0 )
			unitCitcleY = 0.0;
		else
			unitCitcleY = yDistance / onePercent;
		
		unitCircleX = unitCircleX / 100;
		unitCitcleY = unitCitcleY / 100;
	}
	
	function getTouchAndJoystickMiddleDistance()
	{
		return Math.sqrt( Math.pow( ( xZero - x ), 2 ) + Math.pow( ( yZero - y ), 2 ) );
	}
	
	function getCoordsQuadrant( aX, aY )
	{
		if ( aX > xZero && aY < yZero )
		{
			return 1;
		}
		else if ( aX < xZero && aY <= yZero )
		{
			return 2;
		}
		else if ( aX < xZero && aY > yZero )
		{
			return 3;
		}
		else if ( aX > xZero && aY > yZero )
		{
			return 4;
		}
		
		return 0;
	}
	
	function calculateControllerOrigin()
	{
		var distance = getTouchAndJoystickMiddleDistance();
		var touchQuadrant = getCoordsQuadrant( x, y );
		
/* 		Debugger.log( " ---- calculateControllerOrigin ----" ); */
/* 		Debugger.log( "Touch QUADRANT: " + touchQuadrant.toString() ); */
		
		if ( distance < controllerBorderRadius )
		{
			controllerOriginX = x;
			controllerOriginY = y;
		}
		else
		{
			var sX = x - xZero;
			var sY = y - yZero;

			var a = Math.pow( sX, 2 ) + Math.pow( sY, 2 );
			var b = 0
			var c = (-1)* Math.pow( controllerBorderRadius, 2 );
			
			// Discriminant
			var d = Math.pow( b, 2 ) - ( 4 * a * c );
			var D = Math.sqrt( d );
			
/* 			Debugger.log( "b^2: " + Math.pow( b, 2 ) + " - 4ac: " + ( 4 * a * c ) + " = " + d ); */
/* 			Debugger.log( "SQUERE ROOT of d: " + D ); */			
/* 			Debugger.log( "Discriminant: " + D.toString() ); */
				
			var D1 = ( -b + D ) / ( 2 * a );
			var D2 = ( -b - D ) / ( 2 * a );

			
			// X 1,2
			var X1 = xZero + sX*D1;;
			var X2 = xZero + sX*D2;
			
			// Y 1,2
			var Y1 = yZero + sY*D1;
			var Y2 = yZero + sY*D2;
			
			/*
			Debugger.log( "{ X1, Y1 } : { " + X1.toString() + ", " + Y1.toString() + " }");
			Debugger.log( "{ X2, Y2 } : { " + X2.toString() + ", " + Y2.toString() + " }");			
			*/
			var quadrant1 = getCoordsQuadrant( X1, Y1 );
			var quadrant2 = getCoordsQuadrant( X2, Y2 );
			
/* 			Debugger.log( " QUADRANT1: " + quadrant1.toString() + " QUADRANT2: " + quadrant2.toString() ); */
			
			if ( quadrant1 == touchQuadrant )
			{
				controllerOriginX = X1;
				controllerOriginY = Y1;
			}
			else if ( quadrant2 == touchQuadrant )
			{
				controllerOriginX = X2;
				controllerOriginY = Y2;
			}
			else
			{
				Debugger.log( "FATAL ERROR IN CALCULATIONS!" );
				controllerOriginX = xZero;
				controllerOriginY = yZero;
			}
		}
		
/* 		Debugger.log( " ---- calculateControllerOrigin ----" ); */
	}
	
	// send coordination command to robot
	function sendCoords( forceCall )
	{
		if ( serviceURL == null || sessionId == null || sessionId == "Error" || !robotConnection )
		{
			return;
		}
		
		if ( lastCallTimestamp == null )
		{
			lastCallTimestamp = new Date();
			lastCallTimestamp = lastCallTimestamp.setDate( lastCallTimestamp.getDate() );
		}
		
		var currentCallTimestamp = new Date();
		currentCallTimestamp = currentCallTimestamp.setDate( currentCallTimestamp.getDate() );
		
		if ( ( currentCallTimestamp - lastCallTimestamp ) < MIN_COMMANDS_DELAY && !forceCall )
		{
			Debugger.log( 'DROP' );
			return;
		}
	
	
		Debugger.log( 'Sending coords to server: "' + ' x : ' + unitCircleX + ' y : ' + unitCitcleY + '"' );
		
		$.ajax(
		{
			async: true,
			crossDomain: true,
			cache: true,
			type: 'PUT',
			url: serviceURL + '/service/RestRemoteService/sendCommand/',
			contentType: 'application/json',
			headers: { 
						'session-id': sessionId,
						'access-control-request-headers': 'session-id, content-type'
					},
			data: { 'x' : unitCircleX, 'y' : unitCitcleY },
			success: function( data, textStatus, xhr )
				{
                      $('.rtnMsg').html( data );
                       myRtnA = "Success"
                       
                       Debugger.log( jQuery.parseJSON( data ).processed );
                       
                       return myRtnA;
                },
            error: function( xhr, textStatus, errorThrown )
            	{
                       $( '.rtnMsg' ).html( "opps: " + textStatus + " : " + errorThrown );
                       myRtnA = "Error"
                       
                       Debugger.log( textStatus + " " + errorThrown );
                       
                       return myRtnA;
                 }
		});
		
		lastCallTimestamp = currentCallTimestamp;
	}
	
	// light sensor 
	function getLightSensorState()
	{
		if ( serviceURL == null || sessionId == null || sessionId.length <= 0 )
		{
			return;
		}
		
		var waitingDialog = createWaitinDialog(  'Fetching light sensor data ...' );
		
		$.ajax(
		{
			async: true,
			crossDomain: true,
			cache: true,
			type: 'GET',
			url: serviceURL + '/service/RestRemoteService/lightSensor/',
			contentType: 'application/json',
			headers: { 
						'session-id': sessionId,
						'access-control-request-headers': 'session-id, content-type'
					},
			success: function( data, textStatus, xhr )
				{
					waitingDialog.unload();
					
					myRtnA = "Success"
					
					var rtnData = jQuery.parseJSON( data );						
                       
					if ( rtnData == null )
					{
						new Messi( 'Sorry, connection to robot failed.<br/><br/><font color="red">' + jQuery.parseJSON( data ).error_msg + '</font>', { title: 'Error', titleClass: 'anim error', modal: true } );
					}
					else if ( rtnData == "Error" || rtnData == "undefined" )
					{
						new Messi( 'Sorry, it seems to that there were a problem to contact the server, please try again later.', { title: 'Error', titleClass: 'anim error', modal: true } );
					}
					else
					{
						Debugger.log( rtnData.toString() );
						
						var strMsg = "<br/> Measured value: <b>" + rtnData.value + "% </b> of light <br/>";
						
						new Messi( strMsg, { title: 'Light sensor data', titleClass: 'anim info', modal: true } );
						
						drawScreen();
					}
                       
	                 return myRtnA;
                },
            error: function( xhr, textStatus, errorThrown )
            	{	
            		waitingDialog.unload();
            		
					myRtnA = "Error"
                       
					new Messi( 'Sorry, connection to the server failed.', { title: 'Error', titleClass: 'anim error', modal: true } );
                       
					Debugger.log( textStatus + " " + errorThrown );
                       
					return myRtnA;
                 }
		});
	}
	
	// touch sensor 
	function getTouchSensorState()
	{
		if ( serviceURL == null || sessionId == null || sessionId.length <= 0 )
		{
			return;
		}
		
		var waitingDialog = createWaitinDialog(  'Fetching touch sensor state ...' );
		
		$.ajax(
		{
			async: true,
			crossDomain: true,
			cache: true,
			type: 'GET',
			url: serviceURL + '/service/RestRemoteService/touchSensorState/',
			contentType: 'application/json',
			headers: { 
						'session-id': sessionId,
						'access-control-request-headers': 'session-id, content-type'
					},
			success: function( data, textStatus, xhr )
				{
					waitingDialog.unload();
					
					myRtnA = "Success"
					
					var rtnData = jQuery.parseJSON( data );						
                       
					if ( rtnData == null )
					{
						new Messi( 'Sorry, connection to robot failed.<br/><br/><font color="red">' + jQuery.parseJSON( data ).error_msg + '</font>', { title: 'Error', titleClass: 'anim error', modal: true } );
					}
					else if ( rtnData == "Error" || rtnData == "undefined" )
					{
						new Messi( 'Sorry, it seems to that there were a problem to contact the server, please try again later.', { title: 'Error', titleClass: 'anim error', modal: true } );
					}
					else
					{
						Debugger.log( rtnData.toString() );
						
						var strStatus =  ( rtnData.state == true ) ? 'Pushed' : 'Not Pushed';
						
						var strMsg = "<br/> Status: <b>" + strStatus + "</b> <br/>";
						
						new Messi( strMsg, { title: 'Touch sensor status', titleClass: 'anim info', modal: true } );
						
						drawScreen();
					}
                       
	                 return myRtnA;
                },
            error: function( xhr, textStatus, errorThrown )
            	{
            		waitingDialog.unload();
            			
					myRtnA = "Error"
                       
					new Messi( 'Sorry, connection to the server failed.', { title: 'Error', titleClass: 'anim error', modal: true } );
                       
					Debugger.log( textStatus + " " + errorThrown );
                       
					return myRtnA;
                 }
		});
	}
	
	// robot info 
	function getRobotInfo()
	{
		if ( serviceURL == null || sessionId == null || sessionId.length <= 0 )
		{
			return;
		}
		
		var waitingDialog = createWaitinDialog(  'Fetching robot info ...' );
		
		$.ajax(
		{
			async: true,
			crossDomain: true,
			cache: true,
			type: 'GET',
			url: serviceURL + '/service/RestRemoteService/robotInfo/',
			contentType: 'application/json',
			headers: { 
						'session-id': sessionId,
						'access-control-request-headers': 'session-id, content-type'
					},
			success: function( data, textStatus, xhr )
				{
					waitingDialog.unload();
					
					myRtnA = "Success"
					
					var rtnData = jQuery.parseJSON( data );						
                       
					if ( rtnData == null )
					{
						new Messi( 'Sorry, connection to robot failed.<br/><br/><font color="red">' + jQuery.parseJSON( data ).error_msg + '</font>', { title: 'Error', titleClass: 'anim error', modal: true } );
					}
					else if ( rtnData == "Error" || rtnData == "undefined" )
					{
						new Messi( 'Sorry, it seems to that there were a problem to contact the server, please try again later.', { title: 'Error', titleClass: 'anim error', modal: true } );
					}
					else
					{
						Debugger.log( data.toString() );
						
						var strStatus = rtnData.robotInfo.status;
					
						if ( strStatus != "OK" )
						{
							new Messi( 'Robot is in error state!', { title: 'Robot State Error', titleClass: 'anim info', modal: true } );
							return;
						}
						
						var botName = rtnData.robotInfo.bot_name;
						var botAddress = rtnData.robotInfo.bot_address;
						
						var strMsg = "<br/> <b>Status:</b> " + strStatus + "<br/> <b>Name:</b> " + botName + "<br/> <b>Address:</b> " + botAddress + "<br/>";
						
						new Messi( strMsg, { title: 'Robot Info', titleClass: 'anim info', modal: true } );
						
						drawScreen();
					}
                       
	                 return myRtnA;
                },
            error: function( xhr, textStatus, errorThrown )
            	{	
            		waitingDialog.unload();
            	
					myRtnA = "Error"
                       
					new Messi( 'Sorry, connection to the server failed.', { title: 'Error', titleClass: 'anim error', modal: true } );
                       
					Debugger.log( textStatus + " " + errorThrown );
                       
					return myRtnA;
                 }
		});
	}
	
	// connect server with robot via bluetooth
	function connectRobot()
	{
		if ( serviceURL == null || sessionId == null || sessionId.length <= 0 )
		{
			return;
		}

		var waitingDialog = createWaitinDialog(  'Connecting server with robot' );
		
		$.ajax(
		{
			async: true,
			crossDomain: true,
			cache: true,
			type: 'PUT',
			url: serviceURL + '/service/RestRemoteService/bluetooth/',
			contentType: 'application/json',
			headers: { 
						'session-id': sessionId,
						'access-control-request-headers': 'session-id, content-type'
					},
			data: { 'run' : 'true' },
			success: function( data, textStatus, xhr )
				{
					waitingDialog.unload();
					
					myRtnA = "Success"
					
					var running = jQuery.parseJSON( data ).running;
                       
					if ( running == null )
					{
						new Messi( 'Sorry, connection to robot failed.<br/><br/><font color="red">' + data.error_msg + '</font>', { title: 'Error', titleClass: 'anim error', modal: true } );
					}
					else if ( running == "Error" )
					{
						new Messi( 'Sorry, it seems to that there were a problem to connect server to a robot, please try again later.', { title: 'Error', titleClass: 'anim error', modal: true } );
					}
					else if ( running )
					{
/* 						Debugger.log( running.toString() ); */
						robotConnection = running;
						drawScreen();
						setMenuButtons();
					}
					else
					{
						new Messi( 'Sorry, connection to robot failed.<br/><br/><font color="red">' + data.error_msg + '</font>', { title: 'Error', titleClass: 'anim error', modal: true } );
					}
                       
	                 return myRtnA;
                },
            error: function( xhr, textStatus, errorThrown )
            	{	
            		waitingDialog.unload();
            	
					myRtnA = "Error"
                       
					new Messi( 'Sorry, connection to the server failed.', { title: 'Error', titleClass: 'anim error', modal: true } );
                       
					Debugger.log( textStatus + " " + errorThrown );
                       
					return myRtnA;
                 }
		});
	}
	
	
	// connect to server and create session
	function connectToServer()
	{
		if ( serviceURL == null )
		{
			return;
		}
		
		$.ajax(
		{
			async: true,
			crossDomain: true,
			cache: true,
			type: 'PUT',
			url: serviceURL + '/service/RestRemoteService/connect/',
			contentType: 'application/json',
			headers: { 
						'cleint-id': clientId,
						'access-control-request-headers': 'client-id, content-type'
					},
			data: { 'client-id' : clientId },
			success: function( data, textStatus, xhr )
				{
                      $('.rtnMsg').html( data );
                       myRtnA = "Success"
                       
                       Debugger.log( jQuery.parseJSON( data ).sessionId );
                       sessionId = jQuery.parseJSON( data ).sessionId;
                       
                       if ( sessionId == null )
                       {
	                       new Messi( 'Sorry, connection to the server failed.<br/><br/><font color="red">' + data.error_msg + '</font>', { title: 'Error', titleClass: 'anim error', modal: true } );
	                       
	                       $( "#refreshButton" ).css( 'visibility', 'visible' );
	                       $( "#refreshButton" ).click( function()
	                       {
		                       initApp( 'Please enter the URL of RemoteBot service server' );
		                   });
                       }
                       else if ( sessionId == "Error" )
                       {
	                       new Messi( 'Sorry, it seems to that there is another client connected to server, try it later.', { title: 'Error', titleClass: 'anim error', modal: true } );
	                       
	                       $( "#refreshButton" ).css( 'visibility', 'visible' );
	                       $( "#refreshButton" ).click( function()
	                       {
		                       initApp( 'Please enter the URL of RemoteBot service server' );
		                   });
                       }
                       else
                       {
	                       initClientEvents();
	                       connectRobot();
                       }
                       
                       return myRtnA;
                },
            error: function( xhr, textStatus, errorThrown )
            	{
                       $( '.rtnMsg' ).html( "opps: " + textStatus + " : " + errorThrown );
                       myRtnA = "Error"
                       
/*                        drawScreen(); */
                       
                       new Messi( 'Sorry, connection to the server failed.', { title: 'Error', titleClass: 'anim error', modal: true } );
                       
                       $( "#refreshButton" ).css( 'visibility', 'visible' );
                       $( "#refreshButton" ).click( function()
                       {
	                       sessionId = null;
	                       serviceURL = null;
	                       initApp( 'Please enter the URL of RemoteBot service server' );
	                   });
                       
                       Debugger.log( textStatus + " " + errorThrown );
                       
                       return myRtnA;
                 }
		});
	}
	
	/*
		---- INIT APP ----
	*/
	function initApp( message )
	{
		$( '#refreshButton' ).css( 'visibility', 'hidden' );
		
		resetSizes();
				
/* 		new Messi( message + ': <input type="text" name="URL" id="URL" size=50% placeholder="Enter service URL here" value="10.0.1.2:8080"></form>', { title: 'Setup', titleClass: 'anim warning', modal: true, buttons: [{ id: 0, label: 'Submit', val: 'Y' }, { id: 1, label: 'Cancel', val: 'N' }], callback:  */
		new Messi( message + ': <input type="text" name="URL" id="URL" size=50% placeholder="Enter service URL here" value="10.0.14.27:8080"></form>', { title: 'Setup', titleClass: 'anim warning', modal: true, buttons: [{ id: 0, label: 'Submit', val: 'Y' }, { id: 1, label: 'Cancel', val: 'N' }], callback: 
			function( val )
			{ 
				// Handle setup input
				if ( urlInput.value.length > 0 && val.toString() == 'Y' )
				{
					serviceURL = urlInput.value;
					
					if ( serviceURL.substring( 0, 7 ) != 'http://' )
						serviceURL = 'http://' + serviceURL; 
					
/*
					Debugger.log( serviceURL.match( '^(http://)[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\:[0-9]{1,5}' ) );
					Debugger.log( serviceURL.match( '^(http://|www)\S+' ) );
*/
					
					if ( serviceURL.match( '^(http://)[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\:[0-9]{1,5}' ) != null || 
						 serviceURL.match( '^(http://|www)\S+' ) != null )
					{
						if ( serviceURL.charAt( serviceURL.length-1 ) == '/' )
						{
							serviceURL.substring( 0, serviceURL.length - 1 );
						}
						
						initAjax();
						connectToServer();
					}
					else
					{
						serviceURL = null;
						initApp( 'Error, wrong server addres. Please try again' );
						return;
					}
				}
				else
				{
					$( "#refreshButton" ).css( 'visibility', 'visible' );
					$( "#refreshButton" ).click(function()
					{
						initApp( 'Please enter the URL of RemoteBot service server' );
					});
					
					serviceURL = null;
					return;
				}
				
				urlInput = null;
			} 
		});
	
		urlInput = document.getElementById('URL');
		drawScreen();

	}
	
	function setMenuButtons()
	{
		$( "#getBotInfo" ).css( 'visibility', 'visible' );
		$( "#getBotInfo" ).click( function()
		{
			getRobotInfo();
		});
		$( "#getTouchSensor" ).css( 'visibility', 'visible' );
		$( "#getTouchSensor" ).click( function()
		{
			getTouchSensorState()();
		});
		$( "#getLightSensor" ).css( 'visibility', 'visible' );
		$( "#getLightSensor" ).click( function()
		{
			getLightSensorState();
		});
	}
	
	function createWaitinDialog( strTitle )
	{
				
		var waitingDialog = new Messi( '<br/><br/><div id="activity_indicator"></div>', { title: strTitle, titleClass: 'info anim', height:'60px', modal:true } );
		
		var opts = {
			lines: 13, // The number of lines to draw
			length: 7, // The length of each line
			width: 4, // The line thickness
			radius: 10, // The radius of the inner circle
			corners: 1, // Corner roundness (0..1)
			rotate: 0, // The rotation offset
			color: '#0161FF', // #rgb or #rrggbb
			speed: 1, // Rounds per second
			trail: 60, // Afterglow percentage
			shadow: true, // Whether to render a shadow
			hwaccel: false, // Whether to use hardware acceleration
			className: 'spinner', // The CSS class to assign to the spinner
			zIndex: 2e9, // The z-index (defaults to 2000000000)
			top: 'auto', // Top position relative to parent in px
			left: 'auto' // Left position relative to parent in px
		};
		
		var target = document.getElementById('activity_indicator');
		var spinner = new Spinner(opts).spin(target);
		
		return waitingDialog;
	}

	/* 
		---- Drawing content function ----
	*/
   	function drawScreen()
   	{
	   if ( !canvasSupport() )
	   	return;
	   	
	   	// Background fill
        context.fillStyle = "#97D5FF";
        context.fillRect( 0, 0, windowdWidth, windowHeight );
      
      if ( serviceURL != null && serviceURL.length > 0 )
      {
	      // Joystick controller border
	      context.beginPath();
	      context.strokeStyle = "#0000FF";
	      context.lineWidth = 5;
	            
	      context.arc( ( contentRectWidth / 2 ), ( contentRectHeight / 2 ), controllerBorderRadius, ( Math.PI / 180 ) * 0, ( Math.PI / 180 ) * 360, false );
	
	      // Full circle
	      context.stroke();
	      context.closePath();
	        
	      // X axis line
	      context.beginPath();
	        
	      context.lineWidth = 1;
	      context.lineCap  = 'square';
	      context.moveTo( BOX_MARGIN, contentRectHeight / 2 );
	      context.lineTo( contentRectWidth, contentRectHeight / 2 );
	        
	      context.stroke();
	      context.closePath();
	        
	      // Y axis line
	      context.beginPath();
	        
	      context.lineWidth = 1;
	      context.lineCap  = 'square';
	      context.moveTo( contentRectWidth / 2, BOX_MARGIN );
	      context.lineTo( contentRectWidth / 2, contentRectHeight );
	        
	      context.stroke();
	      context.closePath();
	        
	      // Joystick controller
	      context.beginPath();
	        
	      context.globalAlpha = 0.5;
	      context.arc( controllerOriginX, controllerOriginY, ( controllerBorderRadius * 0.15 ),  Math.PI * 0, Math.PI * 2, false );
	
	      context.fillStyle = "#0099FF";
	      context.fill();
	        
	      context.globalAlpha = 0.75;
	      context.strokeStyle = "#3333CC";
	      context.lineWidth = 4;
	      context.stroke();
	      context.globalAlpha = 1.0;
	        
	      context.closePath();
	      
	      
	      context.beginPath();
	      
	      context.globalAlpha = 0.75;
	      context.arc(controllerOriginX, controllerOriginY, 5, 0, 2 * Math.PI, false);
	      context.strokeStyle = '#93B5FF';
	      context.fillStyle='#FF0000';
	      
	      context.fill();
	      context.stroke();
	      
	      context.globalAlpha = 1.0;
	      
	      context.closePath();
	      
	      sendCoords( false );
	        
	      // Debug coords output
	      var coordsTextXMargin = contentRectWidth * 0.025;
	      var coordsTextYMargin = 0;
	        
	      if ( contentRectWidth > contentRectHeight )
	      {
		      coordsTextYMargin = contentRectHeight * 0.75;
	      }
	      else
	      {
		      coordsTextYMargin = contentRectHeight * 0.85;
	      }
	        
	      context.fillStyle = "black";
	      context.font = "italic 30px Calibri";
	      context.fillText  ( 'X: ' + parseFloat( unitCircleX ), coordsTextXMargin, coordsTextYMargin );
	        
	      context.fillStyle = "black";
	      context.font = "italic 30px Calibri";
	      context.fillText  ( 'Y: ' + parseFloat( unitCitcleY ), coordsTextXMargin, coordsTextYMargin + 40 );
	   }
	   else if ( urlInput == null )
	   {
		   new Messi( '&nbsp;<br/>Sorry, you have to input valid service URL to run this application.<br/>&nbsp;', { title: 'Error', titleClass: 'anim error', modal: true } );
	   }
	}
   
	initApp( 'Please enter the URL of RemoteBot service server' );
}