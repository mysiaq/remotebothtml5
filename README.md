RemoteBotClient
==
/*
******************************************************************

Copyright (c) 2013, Michal Bínovský
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are not permitted.

******************************************************************
*/


/* 
   **************************************************************
   ****************************** EN ****************************
   ************************************************************** 
*/
Source code of client application for Diploma Thesis server of application RemoteBot

- source code for theme of diploma thesis: "Controling and monitoring of robot by web services"


/* 
   **************************************************************
   ****************************** SVK ***************************
   ************************************************************** 
*/
Zdrojový kód klientskej aplikácie pre Diplomový projekt aplikácie RemoteBot

- zdrojový kód na diplomový projekt s témou: "Ovládanie a monitorovanie robota pomocou webových služieb"




